//
//  ViewController.swift
//  My First Game - Bulls Eye
//
//  Created by Alina Chernenko on 1/13/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import UIKit
import QuartzCore



class ViewController: UIViewController {
    var CurrentValue = 0
    var targetValue = 0
    var score = 0
    var round = 0
    
    @IBOutlet weak var MySlider: UISlider!
    @IBOutlet weak var gen_num: UITextField!
    @IBOutlet weak var round_field: UITextField!
    @IBOutlet weak var score_field: UITextField!
    
    override func viewDidLoad() {
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")
        MySlider.setThumbImage(thumbImageNormal, forState: .Normal)
        let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")
        MySlider.setThumbImage(thumbImageHighlighted, forState: .Highlighted)
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        if let trackLeftImage = UIImage(named: "SliderTrackLeft") {
            let trackLeftResizable =
            trackLeftImage.resizableImageWithCapInsets(insets)
            MySlider.setMinimumTrackImage(trackLeftResizable, forState: .Normal)
        }
        if let trackRightImage = UIImage(named: "SliderTrackRight") {
            let trackRightResizable =
            trackRightImage.resizableImageWithCapInsets(insets)
            MySlider.setMaximumTrackImage(trackRightResizable, forState: .Normal)
        }
        super.viewDidLoad()
        StartNewRound()
        //UpdateLabels()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func StartNewGame(){
        round=0
        score = 0
        StartNewRound()
    }
    
    func StartNewRound(){
        round++
        targetValue = 1 + Int(arc4random_uniform(100))
        CurrentValue = 1 + Int(arc4random_uniform(100))
        MySlider.value = Float(CurrentValue)
        UpdateLabels()
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseOut)
        view.layer.addAnimation(transition, forKey: nil)
    }
    
    func UpdateLabels(){
        gen_num.text = String(targetValue)
        score_field.text = String (score)
        round_field.text = String (round)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showAlert(){
        
        CurrentValue = lroundf(MySlider.value)
        let Difference = abs(CurrentValue - targetValue)
        var points = (100 - Difference)
        if Difference == 0 {
         points += 100
        } else if Difference == 1 {
            points += 50
        }
        score += points
        
        
        let message = "\nYour score is: \(points) points"
        var title: String
        
        if (Difference == 0) {
            title = "Perfect, you got additional 100 points"
        } else if (Difference == 1) {
            title = "Great Job! You got additional 50 points"
        }
        
        else if (Difference < 5) {
            title = "Alsmost There!"
            
        }else if (Difference < 10) {
            title = "Pretty good, but you can do better!"
        
        } else {
            title = "Not even close..."}
            
            let alert = UIAlertController( title: title,
                message: message,
                preferredStyle: .Alert)
        
        
        let action = UIAlertAction(title: "OK", style: .Default,
            handler: {action in
                self.StartNewRound()
        })
        
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func startover(){
        
        StartNewGame()
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseOut)
        view.layer.addAnimation(transition, forKey: nil)
    }
    

}

